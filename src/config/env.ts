/**
 * 配置文件
 */
export const APP_NAME = '实体通' //名称
export const APP_TYPE = 'wx' //平台类型 wx ks dy
export const BASE_URL = 'https://www-api.zhifuketang.com' //后台根域名
export const API_URL = `${BASE_URL}/prod-api/api/`; //后台接口域名
// export const OSS_URL = `https://henniu-oss.zhifuketang.com/xcx/`; //OSS域名
// export const BASE_URL = 'http://192.168.0.178:6004' //后台根域名
// export const API_URL = `${BASE_URL}/dev-api/api/`; //后台接口域名
// export const IMG_URL = `${BASE_URL}/prod-api`; //全局网络图片地址变量，css背景图片地址变量在style_scss.scss
// export const MAP_KEY = '7c041f15170549c22a3c25049b0bcc2d'; //高德地图开发者key,逆坐标解析
