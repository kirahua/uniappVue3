import {
	aixos,
	silentLogin,
	createParmas,
	authorizationLogin,
	getPhoneNumber,
	getHeader,
	onUpload
} from '@/api/request'
interface API {
	code: number
	cookie: string
	token: string
}




const API = {
	//普通请求
	getList: (params?: unknown) => aixos<{
		code: number
		cookie: string
		token: string
	}>({
		url: 'index/List',
		method: "POST",
		params: params,
		isToken: true
	}),
	//静默登录
	silentLogin: () => silentLogin({
		url: 'third/user/login',
		method: "POST"
	}),

	//获取手机号
	getPhoneNumber: (e: any) => getPhoneNumber({
		url: 'url',
		method: "POST",
		e: e
	}),
	// //生成订单并拉起支付
	createParmas: (orderId: string | number) => createParmas({
		url: 'url',
		method: 'POST',
		orderId: orderId
	})




	// getGoodList(params: any) {
	// 	return 
	// },




	// //授权登录
	// authorizationLogin() {
	// 	return authorizationLogin("third/user/update", "POST")
	// },
	// //更新用户信息
	// updateUserInfo(params: any) {
	// 	return aixos("third/user/update", "POST", params)
	// },
	// //获取手机号
	// getPhoneNumber(e: any) {
	// 	return getPhoneNumber(e, "third/user/updateTel", "POST")
	// },

	// createParmaswx(orderId: string = '') {
	// 	return createParmas("order/add", "POST", orderId)
	// },
	// //请求头 
	// getHeader() {
	// 	return getHeader()
	// },
	// //短信
	// getCode(params: any) {
	// 	return aixos("third/user/sms/sendSms", "POST", params)
	// },
	// //短信验证
	// checkCode(params: any) {
	// 	return aixos("third/user/sms/checkSms", "POST", params)
	// },
	// //提交信息
	// update(params: any) {
	// 	return aixos("third/user/apply/update", "POST", params)
	// },
	// //上传图片
	// onUpload(count: number = 1) {
	// 	return onUpload('file/upload', count)
	// },

	// //订单列表
	// getOrderList(params: any) {
	// 	return aixos("order/list", "GET", params || '')
	// },
}
export default API
