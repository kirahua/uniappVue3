import { defineStore } from 'pinia'
import api from '@/api'
export default defineStore('user', {
    state: () => ({
        userInfo: {}
    }),
    getters: {

    },
    actions: {
        //静默登录
        silentLogin() {
            api.silentLogin().then((res: any) => {
                this.update(res.data)
            })
        },
        //获取手机号
        getPhoneNumber(e: any) {
            api.getPhoneNumber(e).then((res: any) => {
                // this.userInfo.tel = res.data.tel
                // console.log(this.userInfo);
                // uni.setStorageSync("token", res.data.token)
            })
        },
        //更新用户数据
        update(userInfo: any) {
            this.userInfo = userInfo
            uni.setStorageSync("token", userInfo.token)
        }
    }
})