import useAppStore from '@/store/modules/app'
import useUserStore from '@/store/modules/user'
export {
    useAppStore,
    useUserStore
}
import { createPinia } from "pinia"
export default createPinia()