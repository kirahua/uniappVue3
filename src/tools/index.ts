const tools = {
    showtt(title: any, icon: any = 'none') {
        uni.showToast({
            title: title,
            duration: 2000,
            icon: icon
        })
    },
    showload(title: string = '', mask: boolean = false) {
        uni.showLoading({
            title,
            // #ifndef MP-TOUTIAO
            mask
            // #endif 
        })
    },
    // 跳转下一页
    goNext(url: string) {
        uni.navigateTo({
            url: url
        })
    },
    // 返回上一页
    goBack(num: number = 1) {
        uni.navigateBack({
            delta: num
        })
    },
    // 跳转tabar页面
    goSwitchTab(url: string) {
        uni.switchTab({
            url: url
        })
    },
    // 关闭所有页面，跳转到指定页面
    goReLaunch(url: string) {
        uni.reLaunch({
            url: url
        })
    },
    // 关闭当前页，跳转下一页
    goRedirectTo(url: string) {
        uni.redirectTo({
            url: url
        })
    },
    /**
     * 路由传参---对象形式需要
     * @path str
     * @params obj
     */
    goByPath(path: string, params: object, openType: string = 'navigate', animationType: any = 'pop-in',
        animationDuration = 300) {
        // 有参数执行这里的逻辑
        if (params !== undefined && params !== null) {
            if (openType == 'navigate') {
                // 如果跳转方式为navigate，则使用navigateTo方式跳转，保留当前页面，跳转到应用内的某个页面
                uni.navigateTo({
                    url: path + "?params=" + encodeURIComponent(JSON.stringify(params)),
                    animationType: animationType,
                    animationDuration: animationDuration
                })
            } else {
                // 如果跳转方式不为navigate，则使用redirectTo方式跳转，关闭当前页面，跳转到应用内的某个页面
                uni.redirectTo({
                    url: path + "?params=" + encodeURIComponent(JSON.stringify(params))
                })
            }
        } else {
            // 没有参数直接使用navigateTo方式跳转，保留当前页面，跳转到应用内的某个页面
            uni.navigateTo({
                url: path,
                animationType: animationType,
                animationDuration: animationDuration
            })
        }

    },
    //查看图片
    imageLook(httpImgUrl: string[]) {
        uni.previewImage({
            urls: httpImgUrl
        })
    },
    // from表单的输入
    formInfo(data: any, keys: string[]) {
        let obj = {
            ...data
        }
        // data 验证对象 Object
        // keys 验证对象中非必填字段 Array
        if (keys && keys[0]) {
            keys.forEach(val => {
                delete obj[val]

            })
        }
        let show = true
        for (let key in obj) {
            const value = obj[key]
            if (!value) {
                uni.showToast({
                    title: '请检查信息是否填写',
                    icon: 'none'
                })
                console.log('未填写完整', value)
                show = false;
                break;
            }
        }
        return show;
    },
    // 判断身份证号    
    isSfz(idcard: any) {
        var id =
            /^[1-9][0-9]{5}(19|20)[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|31)|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))[0-9]{3}([0-9]|x|X)$/
        if (idcard === null) {
            uni.showToast({
                title: '请输入身份证号',
                icon: 'none'
            })
        } else if (!id.test(idcard)) {
            uni.showToast({
                title: '身份证号格式不正确!',
                icon: 'none'
            })
            return false
        } else {
            return false
        }
    },
    // 判断是否是手机号   
    isPhone(val: any) {
        var patrn = /^(((1[3456789][0-9]{1})|(15[0-9]{1}))+\d{8})$/
        if (!patrn.test(val) || val === '') {
            uni.showToast({
                title: '手机号格式不正确',
                icon: 'none'
            })
            return false
        } else {
            return true
        }
    },
    // 判断邮箱
    isEmail(email: any) {
        if (email.search(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/) != -1)
            return true;
        else
            return false;
    },
    //当前日期
    data() {
        var now = new Date(),
            y = now.getFullYear(),
            m = now.getMonth() + 1,
            d = now.getDate();
        let Time = y + "-" + (m < 10 ? "0" + m : m) + "-" + (d < 10 ? "0" + d : d)
        return Time
    },
    //当前时间
    time() {
        var now = new Date(),
            y = now.getFullYear(),
            m = now.getMonth() + 1,
            d = now.getDate();
        let Time = y + "-" + (m < 10 ? "0" + m : m) + "-" + (d < 10 ? "0" + d : d) + "-" + now.toTimeString()
            .substr(0, 8)
        let str = y + "-" + (m < 10 ? "0" + m : m) + "-" + (d < 10 ? "0" + d : d) + "T" + now.toTimeString().substr(
            0, 8)
        console.log(str, 'str')
        return Time
    },
    //时间转时间戳
    timeTotimeStamp(time: any) {
        let timestamp = Date.parse(new Date(time).toString());
        //timestamp = timestamp / 1000; //时间戳为13位需除1000，时间戳为13位的话不需除1000
        return timestamp;
        //2021-11-18 22:14:24的时间戳为：1637244864707
    },
    //时间差
    // let startime = new Date()
    // let endTime = new Date(res.data.yushou_end_time_text)
    timeDifference(startime: any, endTime: any) {
        const stime = new Date(startime).getTime();
        const etime = new Date(endTime).getTime();
        const diffTime = etime - stime;
        const day = Math.floor(diffTime / (1000 * 60 * 60 * 24));
        const hour = Math.floor(diffTime / (1000 * 60 * 60) % 24);
        const minute = Math.floor(diffTime / (1000 * 60) % 60);
        const second = Math.floor(diffTime / 1000 % 60);
        let obj = {
            day,
            hour,
            minute,
            second,
            timeText: `${day}天${hour}时${minute}分${second}秒`
        }
        return obj
    },
    //时间戳计算时间差
    timestampDifference(startime: any, endTime: any) {
        const diffTime = endTime - startime;
        const day = Math.floor(diffTime / (60 * 60 * 24));
        const hour = Math.floor(diffTime / 60 / 60 % 24);
        const minute = Math.floor(diffTime / 60 % 60);
        const second = Math.floor(diffTime % 60);
        let obj = {
            day,
            hour,
            minute,
            second,
            timeText: `${day}天${hour}时${minute}分${second}秒`
        }
        return obj
    },
    // 时间戳转时间
    timestampToTime(timestamp: any) {
        timestamp *= 1000
        var date = new Date(timestamp); //时间戳为10位需*1000，时间戳为13位的话不需乘1000
        var Y = date.getFullYear() + '-';
        var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
        var D = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) + ' ';
        var h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
        var m = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
        var s = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
        return Y + M + D;
    },
    /**
     * 判断是否是中文
     * @param str
     * @returns {Boolean}
     */
    isChine: function (str: string) {
        var reg = /^([u4E00-u9FA5]|[uFE30-uFFA0])*$/;
        if (reg.test(str)) {
            return false;
        }
        return true;
    },

    isForNumber(num: number) {
        num = Number(num);
        if (num == 0) {
            return num + '';
        } else
            if (num > 1 && num < 10000) {
                return num + '';
            } else {
                return (num / 10000).toFixed(2) + '万';
            }
    },
    /*验证是否为图片*/
    tmCheckImage: function (fileName: any) {
        return /(gif|jpg|jpeg|png|GIF|JPG|PNG)$/ig.test(fileName);
    },
    /*验证是否为视频*/
    tmCheckVideo: function (fileName: any) {
        return /(mp4|mp3|flv|wav)$/ig.test(fileName);
    },
    // 判断密码是否符合 至少6位,包括大小写字母、数字、特殊字符
    isPassword(val: any) {
        var reg = /^(?![0-9]+$)(?![a-z]+$)(?![A-Z]+$)(?!([^(0-9a-zA-Z)])+$)^.{8,16}$/;
        if (val === '') {
            uni.showToast({
                title: '请输入密码',
                icon: 'none'
            })
        } else if (!reg.test(val)) {
            uni.showToast({
                title: '至少6位,包括大小写字母、数字、特殊字符',
                icon: 'none'
            })
            return false
        } else {
            return true
        }
    },
    copyTextH5App(info: any) {
        if (!info) return false;
        info = info + '';
        // #ifndef H5
        uni.setClipboardData({ //准备复制的数据
            data: info + '',
            success: function (res) {
                uni.showToast({
                    title: '复制成功'
                });
            }
        })
        // #endif
        // #ifdef H5
        let result
        let textarea = document.createElement("textarea")
        textarea.value = info
        // textarea.readOnly = "readOnly"
        document.body.appendChild(textarea)
        textarea.select() // 选中文本内容
        textarea.setSelectionRange(0, info.length) // 设置选定区的开始和结束点

        result = document.execCommand("copy") //将当前选中区复制到剪贴板
        // console.log('[result]', result);
        if (result) {
            uni.showToast({ //提示
                title: '复制成功'
            })
        } else {
            uni.showToast({ //提示
                title: '复制失败，请重新尝试',
                icon: 'none'
            })
        }
        textarea.remove()
        // #endif
    },
    /**
     * 节流
     * @path str
     * @params obj
     */
    // debounce(fun: any, time: any) {
    //     let timer: any
    //     return function () {
    //         clearTimeout(timer)
    //         let args = arguments
    //         timer = setTimeout(() => {
    //             fun.apply(this, args)
    //         }, time)
    //     }
    // },
    // throttle(fun: any, time: any) {
    //     let t1 = 0 //初始时间
    //     return function () {
    //         let t2: any = new Date() //当前时间
    //         if (t2 - t1 > time) {
    //             fun.apply(this, arguments)
    //             t1 = t2
    //         }
    //     }
    // },




//     let timer = {}, flag = {};
//     /**
//      * 节流原理：在一定时间内，只能触发一次
//      * 
//      * @param {Function} func 要执行的回调函数 
//      * @param {Number} wait 延时的时间
//      * @param {Boolean} immediate 是否立即执行
//      * @return null
//      */
//     throttle(ns, func, wait = 500, immediate = true) {
//         if (immediate) {
//             if (!flag[ns]) {
//                 flag[ns] = true;
//                 // 如果是立即执行，则在wait毫秒内开始时执行
//                 typeof func === 'function' && func();
//                 timer[ns] = setTimeout(() => {
//                     flag[ns] = false;
//                 }, wait);
//             }
//         } else {
//     if (!flag[ns]) {
//         flag[ns] = true
//         // 如果是非立即执行，则在wait毫秒内的结束处执行
//         timer[ns] = setTimeout(() => {
//             flag[ns] = false
//             typeof func === 'function' && func();
//         }, wait);
//     }

// }
// }
}
export default tools
